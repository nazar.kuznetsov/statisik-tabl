const TerserUglifyPlugin = require('terser-webpack-plugin');
const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack.common');

module.exports = webpackMerge(commonConfig, {
    mode: 'production',
    optimization: {
        minimizer: [
            new TerserUglifyPlugin({
                terserOptions: {
                    compress: {
                        warnings: false
                    },
                    extractComments: true
                },
                sourceMap: true,
                parallel: true
            })
        ]
    },
    devtool: 'source-map',
    resolve: {
        symlinks: false
    }
});
