import {MOCKED_ACTION_SUCCESS, MOCKED_FETCH_ACTION_SUCCESS} from './constants';

const defaultState = {};

const mockReducer = (state: any = defaultState, action: any) => {
    switch (action.type) {
        case MOCKED_FETCH_ACTION_SUCCESS:
            return {
                ...state,
                valueResponse: action.response
            };
        case MOCKED_ACTION_SUCCESS:
            return {
                ...state,
                mockedValue: action.value
            };
        default :
            return state;
    }

};

export {mockReducer};
