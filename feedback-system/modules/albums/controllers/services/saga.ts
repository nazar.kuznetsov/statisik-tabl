import {put, takeEvery} from 'redux-saga/effects';
import {MOCKED_ACTION_SUCCESS, MOCKED_ACTION_REQUEST} from './constants';

function* mockedSetData(action: any) {
    yield put({type: MOCKED_ACTION_SUCCESS, value: action.value});
}

function* mockedSaga() {
    yield takeEvery(MOCKED_ACTION_REQUEST, mockedSetData);
}

export default mockedSaga;
