import {combineReducers} from 'redux';
import {mockReducer} from '../modules/albums/controllers/services/reducers';

const rootReducer = combineReducers({
    mockReducer
});

export default rootReducer;
