import {ApiMethods} from './constants';
import {prepareRequestData} from './prepare-request-data';
import {responseToPromise} from './utils/response-to-promise';

export const request = (callInfo, apiRoot, headers?) => {
    const {method = ApiMethods.GET} = callInfo;

    const pack = prepareRequestData(callInfo, apiRoot, headers);

    return Promise.resolve(fetch(pack.url,
        {
            credentials: 'include',
            method,
            headers: pack.headers,
            body: pack.body
        })
    ).then(response => responseToPromise(response).then(responseData => ({responseData, response})));
};
