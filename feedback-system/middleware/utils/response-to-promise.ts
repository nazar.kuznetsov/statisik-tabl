

import {CONTENT_TYPE_JSON} from '../constants';

const isJson = response => response.headers.get('Content-Type') === CONTENT_TYPE_JSON;

export const responseToPromise = (r: Response): Promise<Object | string> => (isJson(r) ? r.json() : r.text());
