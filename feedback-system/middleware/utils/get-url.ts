import {ApiRequest} from '../typedef';

export const getUrl = (callInfo: ApiRequest, apiRoot: string) => {
    const {webApi = true, endpoint} = callInfo;
    return (webApi && endpoint.indexOf(apiRoot) === -1) ? apiRoot + endpoint : endpoint;
};
