import {CONTENT_TYPE_FORM_DATA} from '../constants';

const prepareRequestQuery = (data: Object) => {
    return ((Object.entries(data): any): Array<[string, any]>)
        .filter(([key, value]) => value !== undefined && value !== null)
        .map(([key, value]) => encodeURIComponent(key) + '=' + encodeURIComponent(value))
        .join('&');
};

type Input = { headers: { [key: string]: string }, params: Object };

type Output = {
    headers: { [key: string]: string },
    body: string
};

export default ({headers, params}: Input): Output => ({
    headers: {
        ...headers,
        'Content-Type': CONTENT_TYPE_FORM_DATA
    },
    body: prepareRequestQuery(params)
});
