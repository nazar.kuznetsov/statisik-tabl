import {CONTENT_TYPE_JSON} from '../constants';

// type Input = { headers: { [key: string]: string }, params: Object | Array<any> };
//
// type Output = {
//     headers: { [key: string]: string },
//     body: string
// };

// @ts-ignore
export default ({headers, params}) => ({
    headers: {
        ...headers,
        'Content-Type': CONTENT_TYPE_JSON
    },
    body: JSON.stringify(params)
});
