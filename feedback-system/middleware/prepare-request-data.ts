import {
    CONTENT_TYPE_JSON,
    TRANSPORT_GET_PARAMS,
    TRANSPORT_REQUEST_BODY,
    TRANSPORT_REQUEST_PARAMS
} from './constants';
import transportRequestBody from './transport/request-body';
import transportRequestParams from './transport/request-params';
import transportGetParams from './transport/get-params';
import {getUrl} from './utils/get-url';

const TRANSPORTS = {
    [TRANSPORT_REQUEST_BODY]: transportRequestBody,
    [TRANSPORT_REQUEST_PARAMS]: transportRequestParams,
    [TRANSPORT_GET_PARAMS]: transportGetParams
};

export const prepareRequestData = (
    callInfo: any,
    apiRoot: any,
    headers = {}
) => {
    return ((Object.entries(callInfo)))
        .filter(([transport]) => TRANSPORTS.hasOwnProperty(transport))
        .reduce((memo, [transport, params]) => ({
            ...memo,
            ...TRANSPORTS[transport]({...memo, params})
        }), {
            headers: {
                'Accept': CONTENT_TYPE_JSON,
                'X-Requested-With': 'XMLHttpRequest',
                ...headers
            },
            url: getUrl(callInfo, apiRoot)
        });
};
