export const CONTENT_TYPE_FORM_DATA = 'application/x-www-form-urlencoded;charset=UTF-8';
export const CONTENT_TYPE_JSON = 'application/json;charset=UTF-8';

export const ApiMethods = {
    CONNECT: 'CONNECT',
    DELETE: 'DELETE',
    GET: 'GET',
    HEAD: 'HEAD',
    OPTIONS: 'OPTIONS',
    POST: 'POST',
    PUT: 'PUT'
};

export const TRANSPORT_REQUEST_BODY = 'TRANSPORT_REQUEST_BODY';
export const TRANSPORT_REQUEST_FILE = 'TRANSPORT_REQUEST_FILE';
export const TRANSPORT_REQUEST_PARAMS = 'TRANSPORT_REQUEST_PARAMS';
export const TRANSPORT_GET_PARAMS = 'TRANSPORT_GET_PARAMS';

export const CALL_API = 'CALL_API';
export const DOWNLOAD_FILE = 'DOWNLOAD_FILE';

export const InternalErrorStatus = {
    SERVICE_UNAVAILABLE: 'SERVICE_UNAVAILABLE',
    CONNECTION_UNABLE: 'CONNECTION_UNABLE'
};

const message = 'There was an issue with your request. Please try again.';

export const INTERNAL_ERROR = {
    httpStatus: 'INTERNAL_SERVER_ERROR',
    message
};
