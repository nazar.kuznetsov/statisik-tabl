import {put, call, takeEvery} from 'redux-saga/effects';
// import {MOCKED_ACTION_SUCCESS, MOCKED_ACTION_REQUEST} from './constants';

function* mockedSetData(action: any) {
    try {
        const data = yield call(fetch, {url: '12321323'});
        yield put({type: 'FETCH_SUCCESS', response: data});
    } catch (e) {
        yield put({type: 'FETCH_FAILURE', value: action.value});
    }
}

function* fetchSaga() {
    yield takeEvery('FETCH_REQUEST', mockedSetData);
}

export default fetchSaga;
