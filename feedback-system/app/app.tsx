import React, {Component, Fragment} from 'react';
import {Switch, Route, Link} from 'react-router-dom';
import {LandingPage} from '../modules/landing-page';
import {Albums} from '../modules/albums';
import {AppBar, Button, Typography} from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';

class App extends Component {
    render() {
        return (
            <Fragment>
                <AppBar position="static" color="default">
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap={true}>
                            Menu
                        </Typography>
                        <Button><Link to="/">Main Pricing</Link></Button>
                        <Button><Link to="/albums">Albums</Link></Button>
                    </Toolbar>
                </AppBar>
                <Switch>
                    <Route path="/albums" component={Albums}/>
                    <Route path="/" component={LandingPage}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
