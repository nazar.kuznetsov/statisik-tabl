import React from 'react';
import {render} from 'react-dom';
import Root from './app/root';
import browserHistory from './history';

const node = document.getElementById('root');

if (node) {
    render(<Root history={browserHistory}/>, node);
}
